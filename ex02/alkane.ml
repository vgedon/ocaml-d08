class alkane n =
object (self)

	inherit Molecule.molecule 
					(* first argument name, construct from names dict.[n] *)
					(let names = ["Méthane" ; "Éthane" ; "Propane" ; "Butane" ; "Pentane" ; "Hexane" ; "Heptane" ; "Octane" ; "Nonane" ; "Décane" ; "Undécane" ; "Dodécane" ; "Tridécane" ; "Tétradécane" ; "Pentadécane" ; "Cétane" ; "Heptadécane" ; "Octadécane" ; "Nonadécane" ; "Eicosane" ; "Heneicosane" ; "Docosane" ; "Tricosane" ; "Tétracosane" ; "Pentacosane" ; "Hexacosane" ; "Heptacosane" ; "Octacosane" ; "Nonacosane" ; "Triacontane" ; "Untriacontane" ; "Dotriacontane" ; "Tritriacontane" ; "Tétratriacontane" ; "Pentatriacontane" ; "Hexatriacontane" ; "Heptatriacontane" ; "Octatriacontane" ; "Nonatriacontane" ; "Tétracontane"]
					in
					let rec loop l acc = match l with
						| hd::tl when n = acc -> hd
						| hd::tl -> loop tl (acc + 1)
						| [] -> ""
					in loop names 1) 
					(* second argument Atom.atom list construct as an alkane (CnH2n+2)*)
					(let rec loop nb acc obj = match nb with
						| 0 -> acc  
						| x -> loop (x-1) (acc@[obj]) obj
					in 
					let hl = loop ((2 * n) +2) [] (new Atom.hydrogen) in
					loop n (hl:>'a list) (new Atom.carbon))

end

class methane =
	object (self)
		inherit alkane 1
	end

class ethane =
	object (self)
		inherit alkane 2
	end


class octane =
	object (self)
		inherit alkane 2
	end

class tetracontane =
	object (self)
		inherit alkane 40
	end

class virtual molecule name (atoms: Atom.atom list) =
object (self)
	val _name:string = name
	val _atoms: Atom.atom list = atoms

	method getAtom (i, a) = a

	method compare_atoms l r = 
		let c = self#getAtom l in
		let d = self#getAtom r in
			if c#atomic_number = 6 then 1
			else if d#atomic_number = 6 then (-1)
			else if c#atomic_number = 1 then 1
			else if d#atomic_number = 1 then (-1)
			else compare c#symbol d#symbol
	method name = _name
	method atoms = _atoms
	method formula = 
		let rec encode lst =
			begin 
			let is_equal (_, c) (_,x) = c#atomic_number = x#atomic_number in
			let incr (i, e) = (i + 1, e) in
			let rec tuplfy l = match l with
				| [] -> []
				| head::tail -> (1 , head):: encode tail
			in 
			let rec compress l = match l with
				| [] -> []
				| head::sec::tail when is_equal head sec -> compress ( incr sec :: tail)
				| head::tail -> head :: compress tail
			in  compress (tuplfy lst)
			end
		in 
		let tuplelist = encode _atoms in
		let sorted = List.sort self#compare_atoms tuplelist in
		let str_of_turple (i, a) = a#symbol ^ if i = 1 then "" else string_of_int i
		in
		let rec stringify t_l acc = 
			match t_l with 
			| [] -> acc
			| hd::bd -> stringify bd (str_of_turple hd ^ acc)
		in stringify sorted ""
		method to_string = self#name ^ " : " ^ self#formula
		method equals (that: molecule) = self#to_string = that#to_string
end




class water =
	object (self)
		inherit molecule "water" [new Atom.hydrogen; new Atom.hydrogen; new Atom.oxygen]
	end

class carbon_dioxyde =
	object (self)
		inherit molecule "carbone dioxyde" [new Atom.carbon; new Atom.oxygen; new Atom.oxygen]
	end

class sucrose =
	object (self)
		inherit molecule "sucrose" [new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;]
	end

class ethanol =
	object (self)
		inherit molecule "ethanol" [new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.oxygen;new Atom.carbon;new Atom.carbon]
	end

class trinitrotoluene =
	object (self)
		inherit molecule "trinitrotoluene" [new Atom.nitrogen;new Atom.nitrogen;new Atom.nitrogen;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.carbon;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.oxygen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen;new Atom.hydrogen]
	end


